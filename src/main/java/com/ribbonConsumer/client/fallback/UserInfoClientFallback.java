package com.ribbonConsumer.client.fallback;
import org.springframework.stereotype.Component;
import com.ribbonConsumer.client.UserInfoClient;
import com.ribbonConsumer.entity.UserPojo;
@Component
public class UserInfoClientFallback implements UserInfoClient{
	@Override
	public String userInfoStr(String name, String age) {
		return "调用失败";
	}

	@Override
	public UserPojo userInfo(String name, String age) {
		//可以设置固定的返回码
		return null;
	}

	@Override
	public String addUserInfo(UserPojo userInfo) {
		//可以设置固定的返回码
		return null;
	}
}
