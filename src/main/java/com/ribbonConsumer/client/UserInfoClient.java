package com.ribbonConsumer.client;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.ribbonConsumer.client.fallback.UserInfoClientFallback;
import com.ribbonConsumer.entity.UserPojo;

/*
 * name：被调用服务的服务名
 * fallback：调用失败之后的回调类
 */
@FeignClient(name = "userInfo-service", fallback = UserInfoClientFallback.class)
public interface UserInfoClient {
	
	/**
	 * userInfo-service服务中的/userInfoStr接口
	 * 写法与被调用的接口一样，只是没有方法体
	 * 注意：
	 * 1、如果通过@RequestParam接收参数，一定要加上(value="xxx")，否则启动报错，
	 * 这是因为spring-cloud-feign处理@RequestParam和Spring mvc的不一样，
	 * Spring mvc在@RequestParam的value为空的时候会通过反射得到参数的名字作为value
	 * 2、如果通过@RequestBody接收map或某个对象，则与普通接口一样
	 */
	@RequestMapping(value = "/userInfoStr", method = RequestMethod.GET)
	public String userInfoStr(@RequestParam(value = "name") String name, @RequestParam(value = "age") String age);
	
	@RequestMapping(value = "/userInfo", method = RequestMethod.GET)
	public UserPojo userInfo(@RequestParam(value = "name") String name, @RequestParam(value = "age") String age);
	
	@RequestMapping(value = "/addUserInfo", method = RequestMethod.POST)
	public String addUserInfo(@RequestBody UserPojo userInfo);
	
	
}
