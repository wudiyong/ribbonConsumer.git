package com.ribbonConsumer.service;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.userInfoService.entity.UserPojo;


@Component
public class MqReceiver {
	@RabbitListener(queues="queue1")
	@RabbitHandler
	public void process1(@Payload UserPojo userPojo) {
		System.out.println("收到消息：name="+userPojo.getName()+",age="+userPojo.getAge());
	}
	
	@RabbitListener(queues="queue2")
	@RabbitHandler
	public void process2(String string) {
		System.out.println("收到消息："+string);
	}
}
